<?php

include_once('transporte.php');

//declaracion de la clase hijo o subclase Helicoptero
class helicoptero extends transporte{

    private $num_helices;

    //declaracion de constructor
		public function __construct($nom,$vel,$com,$heli){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->num_helices=$heli;
				
		}

            // sobreescritura de metodo
    public function resumenHelicoptero(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Helices:</td>
                    <td>'. $this->num_helices.'</td>				
                </tr>';
        return $mensaje;
    }
}


?>